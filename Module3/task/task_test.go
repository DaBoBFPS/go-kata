package main

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"testing"
)

var d = &DoubleLinkedList{
	head: nil,
	tail: nil,
	curr: nil,
	len:  0,
}

const (
	path          = "./testLoadData.json"
	generatedPath = "./GeneratedJson.json"
)

func TestLoadData(t *testing.T) {
	var commit []Commit

	data, err := os.ReadFile(path)
	if err != nil {
		fmt.Println(err)
	}

	err = json.Unmarshal(data, &commit)
	if err != nil {
		panic("Cannot unmarshal the file")
	}

	commit = QuickSort(commit)
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []Commit
		wantErr bool
	}{
		{
			name: "Testing four jsons",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args:    args{path: path},
			want:    commit,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got := d.LoadData(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("LoadData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LoadData() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	var commit Commit

	data, err := os.ReadFile("./testInsert.json")
	if err != nil {
		panic("Cannot read the file")
	}

	err = json.Unmarshal(data, &commit)
	if err != nil {
		panic("Cannot unmarshal the file")
	}
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
		c Commit
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Testing insert function",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args: args{n: 2,
				c: commit},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := d.Insert(tt.args.n, tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	_ = d.LoadData(generatedPath)

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Testing Delete function",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args:    args{n: 2},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := d.Delete(tt.args.n); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	_ = d.LoadData(generatedPath)
	d.curr = d.head

	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name:    "Testing DeleteCurrent function",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := d.DeleteCurrent(); (err != nil) != tt.wantErr {
				t.Errorf("DeleteCurrent() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {
	_ = d.LoadData(path)

	tests := []struct {
		name    string
		want    int
		wantErr bool
	}{
		{
			name:    "Testing Index function",
			want:    0,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := d.Index()
			if (err != nil) != tt.wantErr {
				t.Errorf("Index() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Index() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkDoubleLinkedList_LoadData(b *testing.B) {
	for i := 0; i < b.N; i++ {
		d.LoadData(generatedPath)
	}
}

func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	d.LoadData(generatedPath)
	var commit = &Commit{}
	file, err := os.ReadFile("testInsert.json")
	if err != nil {
		return
	}
	_ = json.Unmarshal(file, commit)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err = d.Insert(0, *commit)
		if err != nil {
			return
		}
	}
}

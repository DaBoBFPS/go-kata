package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	// Make a request to the open weather API to retrieve temperature information
	// and return the result
	// ...
	req := "https://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=" + o.apiKey + "&units=metric"
	resp, err := http.Get(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	var container map[string]interface{}
	_ = json.Unmarshal(body, &container)

	iTemp := container["main"].(map[string]interface{})
	temp := iTemp["temp"].(float64)

	return int(temp)
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	// Make a request to the open weather API to retrieve humidity information
	// and return the result
	// ...
	req := "https://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=" + o.apiKey + "&units=metric"
	resp, err := http.Get(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	var container map[string]interface{}
	_ = json.Unmarshal(body, &container)

	iHumidity := container["main"].(map[string]interface{})
	humidity := iHumidity["humidity"].(float64)

	return int(humidity)
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	// Make a request to the open weather API to retrieve wind speed information
	// and return the result
	// ...
	req := "https://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=" + o.apiKey + "&units=metric"
	resp, err := http.Get(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	var container map[string]interface{}
	_ = json.Unmarshal(body, &container)

	iWind := container["wind"].(map[string]interface{})
	wind := iWind["speed"].(float64)

	return int(wind)
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("a4b5ae80753820621559a3bbbc0a73ba")
	cities := []string{"Москва", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}
}

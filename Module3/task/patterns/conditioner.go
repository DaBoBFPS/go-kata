package main

import "fmt"

type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct{}

func (r *RealAirConditioner) TurnOn() {
	fmt.Println("Turning on the air codtioner")
}

func (r *RealAirConditioner) TurnOff() {
	fmt.Println("Turning off the air codtioner")
}

func (r *RealAirConditioner) SetTemperature(temp int) {
	fmt.Println("Setting air conditioner temperature to 25")
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (r *AirConditionerAdapter) TurnOn() {
	r.airConditioner.TurnOn()
}

func (r *AirConditionerAdapter) TurnOff() {
	r.airConditioner.TurnOff()
}

func (r *AirConditionerAdapter) SetTemperature(temp int) {
	r.airConditioner.SetTemperature(temp)
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func (r *AirConditionerProxy) TurnOn() {
	if r.authenticated {
		r.adapter.TurnOn()
	} else {
		fmt.Println("Access denied: authentication required to turn on the air conditioner")
	}
}

func (r *AirConditionerProxy) TurnOff() {
	if r.authenticated {
		r.adapter.TurnOff()
	} else {
		fmt.Println("Access denied: authentication required to turn off the air conditioner")
	}
}

func (r *AirConditionerProxy) SetTemperature(temp int) {
	if r.authenticated {
		r.adapter.SetTemperature(temp)
	} else {
		fmt.Println("Access denied: authentication required to set the temperature of the air conditioner")
	}
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{

		&AirConditionerAdapter{
			&RealAirConditioner{},
		},
		authenticated,
	}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}

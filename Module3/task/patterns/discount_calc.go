package main

import (
	"fmt"
	"reflect"
)

type Order struct {
	name  string
	price float64
	count int
}

type PricingStrategy interface {
	Calculate(Order) float64
}

type RegularPricing struct {
}

func (r RegularPricing) Calculate(o Order) float64 {
	return o.price * float64(o.count)
}

type SalePricing struct {
	nameSale string
	discount float64
}

func (s SalePricing) Calculate(o Order) float64 {
	return o.price*float64(o.count) - (s.discount*o.price*float64(o.count))/100
}

func main() {
	Prices := []PricingStrategy{
		RegularPricing{},
		SalePricing{
			"Proc",
			5.0,
		},
		SalePricing{
			"Весенняя скидка",
			10.0,
		},
		SalePricing{
			"Регулярн",
			15,
		},
	}
	order := Order{
		"Proc",
		100000,
		10,
	}
	for _, v := range Prices {
		fmt.Println("Total cost with ", reflect.TypeOf(v), " strategy: ", v.Calculate(order))
	}
}

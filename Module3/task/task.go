package main

import ( //для наглядности внизу все методы запущены в мэйне, нужно только path вставить

	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"time"

	gofakeit "github.com/brianvoe/gofakeit"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) []Commit {
	var commit []Commit

	data, err := os.ReadFile(path)
	if err != nil {
		panic("Cannot read the file")
	}

	err = json.Unmarshal(data, &commit)
	if err != nil {
		panic("Cannot unmarshal the file")
	}

	commit = QuickSort(commit)

	for _, c := range commit {
		node := &Node{data: &Commit{
			Message: c.Message,
			UUID:    c.UUID,
			Date:    c.Date,
		}}

		if d.head == nil {
			d.head = node
			d.tail = node
			d.curr = node
		} else {
			d.tail.next = node
			node.prev = d.tail
			d.tail = node
		}

		d.len++
	}

	return commit
}

func QuickSort(commit []Commit) []Commit {
	if len(commit) <= 1 {
		return commit
	}

	median := commit[rand.Intn(len(commit))]

	low_part := make([]Commit, 0, len(commit))
	high_part := make([]Commit, 0, len(commit))
	middle_part := make([]Commit, 0, len(commit))

	for _, item := range commit {
		switch {
		case item.Date.Before(median.Date):
			low_part = append(low_part, item)
		case item.Date.Equal(median.Date):
			middle_part = append(middle_part, item)
		case item.Date.After(median.Date):
			high_part = append(high_part, item)
		}
	}

	low_part = QuickSort(low_part)
	high_part = QuickSort(high_part)

	low_part = append(low_part, middle_part...)
	low_part = append(low_part, high_part...)

	return low_part
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	d.curr = d.curr.next
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	d.curr = d.curr.prev
	return d.curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	var insertNode = &Node{data: &Commit{
		Message: c.Message,
		UUID:    c.UUID,
		Date:    c.Date,
	}}

	if d.Len() == 0 {
		d.head = insertNode
		d.tail = insertNode
	} else {

		for i := 0; i != n; i++ {
			d.Next()
		}

		insertNode.next = d.Current().next
		d.Current().next.prev = insertNode
		insertNode.prev = d.Current()
		d.Current().next = insertNode
	}
	d.len++

	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if d.len == 0 {
		panic(errors.New("List is empty"))
	}

	d.curr = d.head

	for i := 0; i != n; i++ {
		if d.curr.next == nil {
			panic(errors.New("No such Nodes found."))
		}
		d.Next()
	}

	d.curr.prev.next = d.curr.next
	d.curr.next.prev = d.curr.prev
	d.Prev()

	d.len--

	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.len == 0 {
		panic(errors.New("List is empty"))
	}

	switch {
	case d.curr == d.head:
		d.Shift()
	case d.curr == d.tail:
		d.Pop()
	default:
		d.curr.prev.next = d.curr.next
		d.curr.next.prev = d.curr.prev
		d.Prev()
	}

	d.len--

	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.len == 0 {
		panic("No indexes")
	}

	indexNode := d.head
	index := 0

	for indexNode.data != d.curr.data {
		indexNode = indexNode.next
		index++
	}

	return index, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.len == 0 {
		panic("Length is zero")
	}
	d.tail = d.tail.prev
	d.tail.next = nil
	d.curr = d.tail

	d.len--

	return d.tail
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.len == 0 {
		panic("Length is zero")
	}
	d.head = d.head.next
	d.head.prev = nil
	d.curr = d.head

	d.len--

	return d.head
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	if d.len == 0 {
		panic("Length is zero")
	}

	d.curr = d.head

	for d.curr.data.UUID != uuID {
		d.Next()
	}

	return d.curr
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	if d.len == 0 {
		panic("Length is zero")
	}

	d.curr = d.head

	for d.curr.data.Message != message {
		d.Next()
	}

	return d.curr
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	if d.len < 2 {
		return d
	}

	curr := d.head
	for curr != nil {
		curr.prev, curr.next = curr.next, curr.prev
		curr = curr.next
	}

	d.head, d.tail = d.tail, d.head

	d.curr = d.head

	return d
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON(n int) []Commit {
	var commit []Commit

	for i := 0; i < n; i++ {
		var c = &Commit{
			Message: gofakeit.Sentence(10),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.Date(),
		}
		commit = append(commit, *c)
	}

	marshal, err := json.Marshal(commit)
	if err != nil {
		panic("cannot marshal")
	}
	err = os.Chdir("Users\Shampoor\Desktop\sqlite\leetcode\clean\tasj")
	if err != nil {
		panic("Подставь путь")
	}
	err = os.WriteFile("GeneratedJson.json", marshal, 0666)
	if err != nil {
		panic("cannot write file")
	}
	return commit
}

func main() {
	var d DoubleLinkedList
	path := "module3/task/testLoadData.json"
	_, err := d.LoadData(path)
	if err != nil {
		return
	}
	fmt.Println(d)

	var commit Commit

	data, err := os.ReadFile("Users\Shampoor\Desktop\sqlite\leetcode\clean\tasj\testInsert.json")
	if err != nil {
		panic("Cannot read the file")
	}

	err = json.Unmarshal(data, &commit)
	if err != nil {
		panic("Cannot unmarshal the file")
	}

	_ = d.Insert(2, commit)
	fmt.Println("After Insert", d)

	err = d.Delete(2)
	if err != nil {
		return
	}
	fmt.Println("After Delete", d)

	err = d.DeleteCurrent()
	if err != nil {
		return
	}
	d.Next()
	fmt.Println("After DeleteCurrent and Next", d)

	idx, err := d.Index()
	if err != nil {
		return
	}
	fmt.Println("Index of current", idx)

	d.Shift()
	fmt.Println("d Shift", d)

	d.Pop()
	fmt.Println("d POP", d)

	fmt.Println(GenerateJSON(10))
	fmt.Println(d.LoadData("\Users\Shampoor\Desktop\sqlite\leetcode\clean\tasj\GeneratedJson.json"))

	fmt.Println("Before reverse", d)
	fmt.Println("After reverse", d.Reverse())
}

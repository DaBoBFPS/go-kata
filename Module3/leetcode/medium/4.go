package main

func bstToGst(root *TreeNode) *TreeNode {
	resBstToGst(root, 0)

	return root
}

func resBstToGst(root *TreeNode, preVal int) int {
	if root == nil {
		return preVal
	}

	res := resBstToGst(root.Right, preVal)
	root.Val += res
	return resBstToGst(root.Left, root.Val)
}

package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	curr := head
	second := head.Next
	for second.Next != nil {
		if second.Val == 0 {
			curr.Next = second
			curr = curr.Next
		} else {
			curr.Val = curr.Val + second.Val
		}
		second = second.Next
	}
	curr.Next = nil
	return head
}

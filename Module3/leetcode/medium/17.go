package main

import "math"

func countPoints(points [][]int, queries [][]int) []int {
	var answer []int

	for _, circle := range queries {
		counter := 0
		for _, point := range points {
			vector := math.Sqrt(math.Pow(float64(circle[0]-point[0]), 2) + math.Pow(float64(circle[1]-point[1]), 2))
			if float64(circle[2]) >= vector {
				counter++
			}
		}
		answer = append(answer, counter)
	}
	return answer
}

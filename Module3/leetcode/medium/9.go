package main

import "sort"

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	answer := make([]bool, len(l))
	for i := 0; i < len(l); i++ {
		if checkSubArr(&nums, l[i], r[i]) {
			answer[i] = true
		}
	}
	return answer
}

func checkSubArr(nums *[]int, l int, r int) bool {
	ok := true
	numrange := (*nums)[l : r+1]
	arr := make([]int, r-l+1)
	copy(arr, numrange)
	sort.Slice(arr, func(i, j int) bool { return arr[i] < arr[j] })
	d := arr[1] - arr[0]
	for i := 2; i < len(arr); i++ {
		if arr[i]-arr[i-1] != d {
			ok = false
			break
		}
	}
	return ok
}

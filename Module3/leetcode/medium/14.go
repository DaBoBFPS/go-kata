package main

func xorQueries(arr []int, queries [][]int) []int {
	var answer []int
	for _, q := range queries {
		sl := arr[q[0] : q[1]+1]
		xor := arr[q[0]]
		for i := 1; i < len(sl); i++ {
			xor = xor ^ sl[i]
		}
		answer = append(answer, xor)
	}

	return answer
}

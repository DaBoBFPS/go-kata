package main

func averageOfSubtree(root *TreeNode) int {
	counter := 0

	findSum(root, &counter)

	return counter
}

func findSum(root *TreeNode, counter *int) (int, int) {
	total := 0
	num := 0
	if root != nil {
		ltotal, ln := findSum(root.Left, counter)
		rtotal, rn := findSum(root.Right, counter)
		total = ltotal + rtotal + root.Val
		num = ln + rn + 1
		if root.Val == total/num {
			*counter++
		}
	}
	return total, num
}

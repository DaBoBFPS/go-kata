package main

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	visited := make([]bool, n)

	for _, edge := range edges {
		visited[edge[1]] = true
	}

	var ans []int
	for vertex := 0; vertex < n; vertex++ {
		if !visited[vertex] {
			ans = append(ans, vertex)
		}
	}
	return ans
}

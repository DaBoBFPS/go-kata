package main

func pairSum(head *ListNode) int {
	sum := 0
	twin := twins(head)

	for i := 0; i < len(twin)/2; i++ {
		sumTwins := twin[len(twin)-1-i] + head.Val
		if sum < sumTwins {
			sum = sumTwins
		}
		head = head.Next
	}
	return sum
}

func twins(head *ListNode) []int {
	var twins []int
	for head != nil {
		twins = append(twins, head.Val)
		head = head.Next
	}
	return twins
}

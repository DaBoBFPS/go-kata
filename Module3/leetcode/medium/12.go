package main

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	idx := 0

	var result *ListNode
	result = list1

	for idx != a-1 {
		list1 = list1.Next
		idx++
	}

	var old *ListNode
	old = list1.Next
	list1.Next = list2

	for idx != b {
		old = old.Next
		idx++
	}

	for list2.Next != nil {
		list2 = list2.Next
	}
	list2.Next = old

	return result
}

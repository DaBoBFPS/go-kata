package main

func groupThePeople(groupSizes []int) [][]int {

	output := [][]int{}
	m := make(map[int]int)

	for elem, size := range groupSizes {
		if outIndex, ok := m[size]; ok {
			if len(output[outIndex]) < size {
				output[outIndex] = append(output[outIndex], elem)
			} else {
				addNewGroup(elem, size, &output, m)
			}
		} else {
			addNewGroup(elem, size, &output, m)
		}
	}

	return output
}

func addNewGroup(elem, size int, output *[][]int, m map[int]int) {
	temp := []int{}
	temp = append(temp, elem)

	*output = append(*output, temp)
	m[size] = len(*output) - 1
}

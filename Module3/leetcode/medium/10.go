package main

import "fmt"

func numTilePossibilities(tiles string) int {
	wordSet := make(map[string]struct{})

	var queue []string

	for i := 0; i < len(tiles); i++ {
		char := fmt.Sprintf("%c", tiles[i])
		length := len(queue)
		wordSet[char] = struct{}{}
		queue = append(queue, char)

		for j := 0; j < length; j++ {
			for k := 0; k <= len(queue[j]); k++ {
				item := queue[j]
				item = item[:k] + char + item[k:]
				wordSet[item] = struct{}{}
				queue = append(queue, item)
			}
		}
	}

	return len(wordSet)
}

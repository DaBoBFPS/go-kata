package main

import "math"

func maxSum(grid [][]int) int {

	r := len(grid) - 2
	c := len(grid[0]) - 2

	maxSum := math.MinInt

	for i := 0; i < r; i++ {
		for j := 0; j < c; j++ {
			sum := grid[i][j] + grid[i][j+1] + grid[i][j+2] + grid[i+1][j+1] + grid[i+2][j] + grid[i+2][j+1] + grid[i+2][j+2]

			if sum > maxSum {
				maxSum = sum
			}
		}
	}

	return maxSum
}

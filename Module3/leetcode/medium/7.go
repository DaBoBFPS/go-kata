package main

func balanceBST(root *TreeNode) *TreeNode {
	arr := make([]int, 0)

	inorderTrav(root, &arr)

	return sortedArrToBST(arr, 0, len(arr)-1)
}

func inorderTrav(root *TreeNode, arr *[]int) {
	if root == nil {
		return
	}
	inorderTrav(root.Left, arr)
	*arr = append(*arr, root.Val)
	inorderTrav(root.Right, arr)
}

func sortedArrToBST(arr []int, start int, end int) *TreeNode {
	if start < 0 || start >= len(arr) || end < 0 || end >= len(arr) || start > end {
		return nil
	}

	mid := start + (end-start)/2

	node := new(TreeNode)
	node.Val = arr[mid]

	node.Left = sortedArrToBST(arr, start, mid-1)
	node.Right = sortedArrToBST(arr, mid+1, end)

	return node
}

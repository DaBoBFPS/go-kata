package main

func numIdenticalPairs(nums []int) int {
	Pairs := 0

	for i := range nums {
		for j := range nums {
			if nums[i] == nums[j] && i < j {
				Pairs++
			}
		}
	}
	return Pairs
}

package leetcode

func kidsWithCandies(candies []int, extraCandies int) []bool {

	max := 0
	for i := 0; i < len(candies); i++ {
		if max < candies[i] {
			max = candies[i]
		}
	}
	candeez := make([]bool, len(candies))
	for i, candy := range candies {
		if candy+extraCandies >= max {
			candeez[i] = true
		}
	}
	return candeez
}

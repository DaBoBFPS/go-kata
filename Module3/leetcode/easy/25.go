package leetcode

func createTargetArray(nums []int, index []int) []int {
	m := make([]int, 0)
	for i, v := range index {
		if v >= len(m) {
			m = append(m, nums[i])
		} else {
			m = append(m[:v], append([]int{nums[i]}, m[v:]...)...)
		}
	}
	return m
}

package leetcode

func decode(encoded []int, first int) []int {
	decoded := make([]int, 0)
	decoded = append(decoded, first)
	for i, v := range encoded {
		decoded = append(decoded, decoded[i]^v)
	}
	return decoded
}

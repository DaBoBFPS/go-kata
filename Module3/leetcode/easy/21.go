package leetcode

func subtractProductAndSum(n int) int {
	sum, mult := 0, 1
	for n > 0 {
		sum += n % 10
		mult *= n % 10
		n = n / 10

	}
	return mult - sum
}

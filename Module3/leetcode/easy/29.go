package main

func xorOperation(n int, start int) int {

	sum := start
	for i := 1; i < n; i++ {
		num := start + 2*i
		sum ^= num
	}
	return sum
}

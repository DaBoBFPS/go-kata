package main

func temperature(celsius float64) []float64 {
	var temp = make([]float64, 2)
	temp[0] = celsius + 273.15
	temp[1] = celsius*1.80 + 32.00
	return temp
}

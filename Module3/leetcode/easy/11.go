package leetcode

func runningSum(nums []int) []int {
	sum := make([]int, len(nums))

	for i := 0; i < len(nums); i++ {
		suma := 0
		for j := 0; j < i+1; j++ {
			suma += nums[j]
		}
		sum[i] = suma
	}
	return sum
}

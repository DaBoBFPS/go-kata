package main

func numberOfMatches(n int) int {
	matches := 0
	match := 0

	for n != 1 {
		if n%2 == 0 {
			match = (n - 1) / 2
		} else {
			match = (n - 1) / 2
		}
		n -= match
		matches += match
	}
	return matches
}

package leetcode

func maximumWealth(accounts [][]int) int {
	Maj := 0

	for _, w := range accounts {
		rich := 0
		for _, r := range w {
			rich += r
		}
		if rich > Maj {
			rich = Maj
		}
	}
	return Maj
}

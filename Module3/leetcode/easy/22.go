package leetcode

func smallerNumbersThanCurrent(nums []int) []int {
	small := make([]int, len(nums))
	for i, v := range nums {
		counter := 0
		for k, val := range nums {
			if val != v && nums[k] < nums[i] {
				counter++
			}
		}
		small[i] = counter
	}
	return small
}

package leetcode

func mostWordsFound(sentences []string) int {
	maxNumWords := 0
	for _, s := range sentences {
		counter := 0
		for _, l := range s {
			if string(l) == " " {
				counter++
			}
		}
		if maxNumWords < counter {
			maxNumWords = counter
		}
	}
	return maxNumWords
}

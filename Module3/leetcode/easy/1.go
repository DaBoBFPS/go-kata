package main

func tribonacci(n int) int {
	var TriboSlice = []int{0, 1, 1}
	if n < 3 {
		return TriboSlice[n]
	}
	for i := 3; i < n+1; i++ {
		tn := TriboSlice[i-3] + TriboSlice[i-2] + TriboSlice[i-1]
		TriboSlice = append(TriboSlice, tn)
	}
	return TriboSlice[len(TriboSlice)-1]
}

package leetcode

func interpret(command string) string {
	output := ""

	for i, v := range command {
		switch {
		case v == 'G':
			output += string(v)
		case v == '(' && command[i+1] == ')':
			output += "o"
		case v == '(' && command[i+1] == 'a':
			output += "al"
		}
	}
	return output
}

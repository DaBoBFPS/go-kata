package leetcode

func finalValueAfterOperations(operations []string) int {
	X := 0

	// map + and - to values
	values := map[byte]int{'+': 1, '-': -1}

	for _, operation := range operations {
		// only need to check middle character
		// and mapping of the byte
		X += values[operation[1]]
	}

	return X
}


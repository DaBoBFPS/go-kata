package leetcode

func differenceOfSum(nums []int) int {
	sumElem := 0
	sumNums := 0
	absDiff := 0
	for _, num := range nums {
		sumElem += num
		if num > 9 && num < 100 {
			leftNum := num / 10
			rightNum := num % 10
			sumNums += leftNum + rightNum

		} else if num > 99 && num < 1000 {
			leftNum := num / 100
			rightNum := num % 10
			midNum := (num - leftNum*100) / 10
			sumNums += leftNum + midNum + rightNum
		} else if num > 999 {
			leftNum := num / 1000
			midNumLeft := (num - leftNum*100) / 100
			midNumRight := (num - leftNum*1000 - midNumLeft*100) / 10
			rightNum := num % 10
			sumNums += leftNum + rightNum + midNumLeft + midNumRight
		} else {
			sumNums += num
		}

	}
	absDiff = sumNums - sumElem
	if absDiff > 0 {
		absDiff = absDiff * -1
	}
	return absDiff
}

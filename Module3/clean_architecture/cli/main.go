package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/Module3/clean_architecture/service/repo"
	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/Module3/clean_architecture/service/service"
)

func main() {
	fmt.Println(`
	Hello there! I am a TODO tasker, here are my capabilities:
	1. Type show and I'll give you the list of TODO tasks you already have. You'll get error if it's empty.
	2. Type add, press Enter and then add the description and ID to add a new task.
	3. Type remove, press Enter and then type the ID number of the task to delete it.
	4. Type complete, press Enter and then type the ID number of the task to complete it
	5. Type exit and I'll get you out of here.`)

	var action string
	local := repo.NewTodoLocalStorage()
	todos := service.NewTodoService(local)
	reader := bufio.NewReader(os.Stdin)

	for action != "exit" {
		_, err := fmt.Scanln(&action)
		if err != nil {
			log.Fatal("Check the input")
		}

		switch {
		case action == "show":
			todos, _ := todos.ListTodos()
			fmt.Println("Here'are your TODOs:")
			for _, todo := range todos {
				fmt.Printf("Id: %d, Description: %s, Created at: %v \n", todo.ID, todo.Description, todo.Created)
			}
		case action == "add":
			var id string
			fmt.Println("Type description:")
			description, _ := reader.ReadString('\n')
			fmt.Println("Type ID:")
			_, _ = fmt.Scanln(&id)
			newid, _ := strconv.Atoi(id)
			_ = todos.CreateTodo(newid, description)
			fmt.Println("Successfully added task")
		case action == "remove":
			var id string
			fmt.Println("Type ID:")
			_, _ = fmt.Scanln(&id)
			newid, _ := strconv.Atoi(id)
			_ = todos.RemoveTodo(newid)
			fmt.Println("Successfully removed task")
		case action == "complete":
			var id string
			fmt.Println("Type ID:")
			_, _ = fmt.Scanln(&id)
			newid, _ := strconv.Atoi(id)
			_ = todos.CompleteTodo(newid)
			fmt.Println("Successfully completed task, nice job!")
		}
	}
}

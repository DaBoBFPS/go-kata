package test

import (
	"testing"
	"time"

	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/Module3/clean_architecture/service/repo"

	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/Module3/clean_architecture/service/model"
)

func TestTodoLocalStorage_CompleteTodo(t *testing.T) {
	type fields struct {
		todos map[int]*model.Todo
	}
	type args struct {
		todo int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Testing CompleteTodo function",
			fields: fields{todos: map[int]*model.Todo{
				0: {
					ID:          0,
					Description: "Punch Vlad in the face",
					Created:     time.Now(),
				},
				1: {
					ID:          1,
					Description: "Finish Kata course",
					Created:     time.Now(),
				},
			}},
			args:    args{todo: 1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &repo.TodoLocalStorage{
				Todos: tt.fields.todos,
			}
			if err := r.CompleteTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTodoLocalStorage_CreateTodo(t *testing.T) {
	type fields struct {
		todos map[int]*model.Todo
	}
	type args struct {
		todo *model.Todo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "testing CreateTodo function",
			fields: fields{todos: map[int]*model.Todo{
				0: {
					ID:          0,
					Description: "Punch Vlad in the face",
					Created:     time.Now(),
				}}},
			args: args{todo: &model.Todo{
				ID:          1,
				Description: "Finish Kata course",
				Created:     time.Now(),
			}},
			wantErr: false,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &repo.TodoLocalStorage{
				Todos: tt.fields.todos,
			}
			if err := r.CreateTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTodoLocalStorage_ListTodos(t *testing.T) {
	type fields struct {
		todos map[int]*model.Todo
	}
	tests := []struct {
		name   string
		fields fields
		want   []*model.Todo
	}{
		{
			name: "testing ListTodos function",
			fields: fields{todos: map[int]*model.Todo{
				0: {
					ID:          0,
					Description: "Punch Vlad in the face",
					Created:     time.Now(),
				},
				1: {
					ID:          1,
					Description: "Finish Kata course",
					Created:     time.Now(),
				},
			},
			},
			want: []*model.Todo{
				{
					ID:          0,
					Description: "Punch Vlad in the face",
					Created:     time.Now(),
				},
				{
					ID:          1,
					Description: "Finish Kata course",
					Created:     time.Now(),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &repo.TodoLocalStorage{
				Todos: tt.fields.todos,
			}
			if got := r.ListTodos(); len(got) != len(tt.want) {
				t.Errorf("ListTodos() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTodoLocalStorage_RemoveTodo(t *testing.T) {
	type fields struct {
		todos map[int]*model.Todo
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{name: "testing RemoveTodo function",
			fields: fields{todos: map[int]*model.Todo{
				0: {
					ID:          0,
					Description: "Punch Vlad in the face",
					Created:     time.Now(),
				},
				1: {
					ID:          1,
					Description: "Finish Kata course",
					Created:     time.Now(),
				},
			},
			},
			args:    args{id: 1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &repo.TodoLocalStorage{
				Todos: tt.fields.todos,
			}
			if err := r.RemoveTodo(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

package test

import (
	"reflect"
	"testing"

	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/Module3/clean_architecture/service/repo"
	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/Module3/clean_architecture/service/service"

	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/Module3/clean_architecture/service/model"
)

func TestTodoUseCase_CompleteTodo(t *testing.T) {
	type fields struct {
		todoRepo repo.TodoRepository
	}
	type args struct {
		todo int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "testing completetodo",
			fields:  fields{todoRepo: repo.NewTodoLocalStorage()},
			args:    args{todo: 1},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoUseCase{
				TodoRepo: tt.fields.todoRepo,
			}
			if err := s.CompleteTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTodoUseCase_CreateTodo(t *testing.T) {
	type fields struct {
		todoRepo repo.TodoRepository
	}
	type args struct {
		id          int
		description string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "testing createtodo",
			fields: fields{todoRepo: repo.NewTodoLocalStorage()},
			args: args{
				id:          0,
				description: "Punching the box",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoUseCase{
				TodoRepo: tt.fields.todoRepo,
			}
			if err := s.CreateTodo(tt.args.id, tt.args.description); (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTodoUseCase_ListTodos(t *testing.T) {
	type fields struct {
		todoRepo repo.TodoRepository
	}
	tests := []struct {
		name    string
		fields  fields
		want    []*model.Todo
		wantErr bool
	}{
		{
			name:    "testing listtodos",
			fields:  fields{todoRepo: repo.NewTodoLocalStorage()},
			want:    []*model.Todo{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoUseCase{
				TodoRepo: tt.fields.todoRepo,
			}
			got, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListTodos() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTodoUseCase_RemoveTodo(t *testing.T) {
	type fields struct {
		todoRepo repo.TodoRepository
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "testing removetodo",
			fields:  fields{todoRepo: repo.NewTodoLocalStorage()},
			args:    args{id: 0},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoUseCase{
				TodoRepo: tt.fields.todoRepo,
			}
			if err := s.RemoveTodo(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

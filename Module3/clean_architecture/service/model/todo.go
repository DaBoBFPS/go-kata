package model

import "time"

type Todo struct {
	ID          int
	Description string
	Created     time.Time
}

package service

import (
	"time"

	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/Module3/clean_architecture/service/repo"

	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/Module3/clean_architecture/service/model"
)

type TodoService interface {
	ListTodos() ([]*model.Todo, error)
	CreateTodo(id int, description string) error
	CompleteTodo(id int) error
	RemoveTodo(id int) error
}

type TodoUseCase struct {
	TodoRepo repo.TodoRepository
}

func NewTodoService(todoRepo repo.TodoRepository) *TodoUseCase {
	return &TodoUseCase{TodoRepo: todoRepo}
}

func (s *TodoUseCase) ListTodos() ([]*model.Todo, error) {
	return s.TodoRepo.ListTodos(), nil
}

func (s *TodoUseCase) CreateTodo(id int, description string) error {
	todo := &model.Todo{
		ID:          id,
		Description: description,
		Created:     time.Now(),
	}

	return s.TodoRepo.CreateTodo(todo)
}

func (s *TodoUseCase) CompleteTodo(id int) error {
	return s.TodoRepo.CompleteTodo(id)
}

func (s *TodoUseCase) RemoveTodo(id int) error {
	return s.TodoRepo.RemoveTodo(id)
}

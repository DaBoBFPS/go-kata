package main

import (
	"encoding/json"
	"io"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	FilePath string
}

func NewUserRepository(dir string) *UserRepository {
	return &UserRepository{
		FilePath: dir,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	var user []User

	_, err := os.OpenFile(r.FilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	data, err := os.ReadFile(r.FilePath)
	if err != nil {
		return err
	}

	if len(data) != 0 {
		err = json.Unmarshal(data, &user)
		if err != nil {
			return err
		}
	}

	newUser := User{
		ID:   record.(User).ID,
		Name: record.(User).Name,
	}

	user = append(user, newUser)

	jsonRecord, err := json.Marshal(user)
	if err != nil {
		return err
	}

	if err := os.WriteFile(r.FilePath, jsonRecord, 0666); err != nil {
		return err
	}
	// logic to write a record as a JSON object to a file at r.FilePath
	return err
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	var user []map[string]interface{}

	users, err := r.FindAll()
	if err != nil {
		return user, err
	}

	for _, u := range users {
		for k, v := range u.(map[string]interface{}) {
			if k == "id" && v.(float64) == float64(id) {
				return u, nil
			}
		}
	}
	return user, nil
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	var users []interface{}

	file, err := os.Open(r.FilePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(content, &users); err != nil {
		return nil, err
	}

	return users, nil
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
}

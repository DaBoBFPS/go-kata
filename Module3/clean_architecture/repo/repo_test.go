package clean

import (
	"reflect"
	"testing"
)

func TestUserRepository_Save(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		record interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "Testing save function",
			fields: fields{FilePath: "./test.json"},
			args: args{User{
				ID:   0,
				Name: "Jason",
			}},
			wantErr: false,
		},
		{
			name:   "Testing save function",
			fields: fields{FilePath: "./test.json"},
			args: args{User{
				ID:   1,
				Name: "Data",
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := r.Save(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	type fields struct {
		FilePath string
	}
	tests := []struct {
		name    string
		fields  fields
		want    []interface{}
		wantErr bool
	}{
		{
			name:    "Testing FinAll function",
			fields:  fields{FilePath: "./test.json"},
			want:    []interface{}{map[string]interface{}{"id": float64(0), "name": "Jason"}, map[string]interface{}{"id": float64(1), "name": "Data"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_Find(t *testing.T) {
	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name:    "Testing find function",
			fields:  fields{FilePath: "./test.json"},
			args:    args{0},
			want:    map[string]interface{}{"id": float64(0), "name": "Jason"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() got = %v, want %v", got, tt.want)
			}
		})
	}
}

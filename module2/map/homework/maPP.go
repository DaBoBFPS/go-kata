package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/docker/roadmap",
			Stars: 1200,
		},
		{
			Name:  "https://github.com/docker/awesome-compose",
			Stars: 22200,
		},
		{
			Name:  "https://github.com/docker/build-push-action",
			Stars: 3200,
		},
		{
			Name:  "https://github.com/docker/compose-cli",
			Stars: 916,
		},
		{
			Name:  "https://github.com/docker/extensions-sdk",
			Stars: 105,
		},
		{
			Name:  "https://github.com/docker/docs",
			Stars: 3800,
		},
		{
			Name:  "https://github.com/docker/cli",
			Stars: 3900,
		},
		{
			Name:  "https://github.com/docker/buildx",
			Stars: 2500,
		},
		{
			Name:  "https://github.com/docker/containerd-packaging",
			Stars: 53,
		},
		{
			Name:  "https://github.com/docker/login-action",
			Stars: 656,
		},
		{
			Name:  "https://github.com/docker/getting-started",
			Stars: 2600,
		},
		{
			Name:  "https://github.com/docker/docker-ce-packaging",
			Stars: 158,
		},
	}
	m := make(map[string]int)
	for _, v := range projects {
		m[v.Name] = v.Stars

	}
	for k, v := range m {
		fmt.Println(k, v)
	}
}

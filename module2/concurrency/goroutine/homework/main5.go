package main

import (
	"fmt"
	"time"
)

func main() {
	message1 := make(chan string)
	message2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Microsecond * 500)
			message1 <- "прошло пол секунды"
		}
	}()
	go func() {
		for {
			time.Sleep(time.Microsecond * 2)
			message1 <- "прошло две секунды"
		}
	}()
	for {
		select {
		case msg := <-message1:
			fmt.Println(msg)
		case msg := <-message2:
			fmt.Println(msg)

		}
	}
}

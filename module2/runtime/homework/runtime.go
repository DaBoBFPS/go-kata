package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("Я могу юзать")
	go func() {
		fmt.Println("горутины в GO")
	}()
	runtime.Gosched()
	fmt.Println("Эт збс, понял наконец то!")
}

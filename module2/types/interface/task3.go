package main

import (
	"fmt"
)

type User struct {
	ID   int
	Name string
}

func (u User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func convert(u []User) []Userer {
	i := []Userer{}
	for _, v := range u {
		i = append(i, v)
	}
	return i
}

func main() {
	u := []User{
		{
			ID:   34,
			Name: "Annet",
		},
		{
			ID:   55,
			Name: "John",
		},
		{
			ID:   89,
			Name: "Alex",
		},
	}
	users := convert(u)
	testUserName(users)
}

func testUserName(users []Userer) {
	for _, u := range users {
		fmt.Println(u.GetName())
	}
}

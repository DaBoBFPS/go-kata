package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch t := r.(type) {
	case *int:
		if t == nil {
			fmt.Println("Success!")
		}
	}
}

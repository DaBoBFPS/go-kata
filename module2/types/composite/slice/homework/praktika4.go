package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func shift(a []User) []User {
	b := a[1:]
	return b
}

func pop(a []User) []User {
	b := a[:len(a)-1]
	return b
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	users = pop(users)
	users = shift(users)
	fmt.Println(users)
}

package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func sort(a []User) []User {
	u := []User{}
	for i := 0; i < len(a); i++ {
		t := a[i]
		if t.Age < 40 {
			u = append(u, t)
		}
	}
	return u
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	users = sort(users)
	fmt.Println(users)
}

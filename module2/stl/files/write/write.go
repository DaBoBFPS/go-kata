package main

import (
	"fmt"
	"io"
	"os"
)

func ReplaceAll(st []rune, old rune, new string) []rune {
	new_rune := []rune(new)
	le_new := len(new_rune)
	for i, k := range st {
		if k == old {
			st = append(st[:i], st[i:]...)
			for j := 0; j < le_new; j++ {
				st[i+j] = new_rune[j]
			}
		}
	}
	return st
}

var baseRuEn = map[string]string{
	"а": "a", "А": "A", "Б": "B", "б": "b", "В": "V", "в": "v", "Г": "G", "г": "g",
	"Д": "D", "д": "d", "З": "Z", "з": "z", "И": "I", "и": "i", "К": "K", "к": "k",
	"Л": "L", "л": "l", "М": "M", "м": "m", "Н": "N", "н": "n", "О": "O", "о": "o",
	"П": "P", "п": "p", "Р": "R", "р": "r", "С": "S", "с": "s", "Т": "T", "т": "t",
	"У": "U", "у": "u", "Ф": "F", "ф": "f", "ч": "ch", "Ч": "ch", "ы": "y", "я": "ya", "Я": "Ya",
	"щ": "sh", "Щ": "Sh", "ш": "sh", "Ш": "Sh", "ю": "u", "Ю": "U", "ь": "`", "Ь": "`",
	"ц": "c", "Ц": "C", "ж": "zh", "Ж": "Zh", "й": "i", "Й": "I", "э": "e", "Э": "E",
}

func ToLatin(s string) string {
	runes := []rune(s)
	for i := range runes {
		runes = ReplaceAll(runes, runes[i], baseRuEn[string(runes[i])])
	}

	return string(runes)
}

func main() {
	file, err := os.Open("example.txt")
	if err != nil {
		fmt.Println("Unable to Open file:", err)
		os.Exit(1)
	}
	defer file.Close()
	data := []byte{}
	for {
		data2 := make([]byte, 32)
		_, err := file.Read(data2)
		if err == io.EOF {
			break
		}
		data = append(data, data2...)
	}

	b := []rune(string(data))
	lat := ToLatin(string(b))

	out, err := os.Create("example.processed.txt")

	if err != nil {
		fmt.Println("Unable to create file:", err)
		os.Exit(1)
	}

	defer out.Close()
	fmt.Println(lat)
	out.Write([]byte(lat))

}

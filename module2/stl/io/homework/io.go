package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	file, _ := os.Create("example.txt")
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	buf := bytes.Buffer{}
	for _, i := range data {
		buf.Write([]byte(i))
		buf.WriteString("\n")
	}
	file.Write(buf.Bytes())
	file.Close()
	file_read, _ := os.Open("example.txt")
	buf_read := bytes.Buffer{}
	for {
		d := make([]byte, 32)
		_, err := file_read.Read(d)
		if err == io.EOF {
			break
		}
		buf_read.Write(d)
	}
	file_read.Close()
	fmt.Println(buf_read.String())

}

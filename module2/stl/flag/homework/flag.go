package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func main() {
	filePath := flag.String("conf", "", "path to the json file")
	flag.Parse()

	if *filePath != "" {
		data, err := os.ReadFile(*filePath)
		if err != nil {
			fmt.Println("error")
			os.Exit(1)
		}

		conf := Config{}
		fmt.Printf("Error: %v\n", json.Unmarshal(data, &conf))
		fmt.Printf("%#v\n", conf)
	} else {
		fmt.Println("You need to specify a path")
		os.Exit(1)
	}
}

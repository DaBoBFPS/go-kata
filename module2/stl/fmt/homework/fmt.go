package main

import (
	"fmt"
)

func GenerateSelfStory(name string, age int, money float32) string {
	return "Hello, my name is " + name + ". I'm " + string(age) + " y.o. " + "And I also have " + fmt.Sprintf("%.2f", money) + " in my wallet right now."

}
func main() {
	var name string
	var age int
	var money float32
	fmt.Print("Введите Имя: ")
	fmt.Scanln(&name)
	fmt.Print("Введите Возраст: ")
	fmt.Scanln(&age)
	fmt.Print("Введите Количество денег: ")
	fmt.Scanln(&money)
	fmt.Println(GenerateSelfStory(name, age, money))
}

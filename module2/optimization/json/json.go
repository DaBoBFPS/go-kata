package benching

import (
	"encoding/json"
	"testing"

	jsoniter "github.com/json-iterator/go"
)

var jsonData = []byte(`[
{
"id": 9223372036854610000,
"category": {
"id": 0,
"name": "dog"
},
"name": "doggie",
"photoUrls": [
"string"
],
"tags": [
{
"id": 0,
"name": "string"
}
],
"status": "pending"
},
{
"id": 12345,
"category": {
"id": 1,
"name": "dog"
},
"name": "snoopie",
"photoUrls": [
"string"
],
"tags": [
{
"id": 0,
"name": "string"
}
],
"status": "pending"
}
]`)

var toJsonData = Pets{
	Pet{
		ID:        0,
		Category:  Category{ID: 1, Name: "name"},
		Name:      "Catty",
		PhotoUrls: nil,
		Tags:      nil,
		Status:    "dangling",
	}, Pet{
		ID:        1,
		Category:  Category{ID: 1, Name: "name"},
		Name:      "Agnes",
		PhotoUrls: nil,
		Tags:      nil,
		Status:    "available",
	},
}

type Pets []Pet

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func Marshal(r Pets) ([]byte, error) {
	return json.Marshal(r)
}

type Pet struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func UnmarshalPets2(data []byte) (Pets, error) {
	var p Pets
	err := jsoniter.Unmarshal(data, &p)
	return p, err
}

func Marshal2(r Pets) ([]byte, error) {
	return jsoniter.Marshal(r)
}

func BenchmarkStandardJsonUnmarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkStandardJsonMarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := Marshal(toJsonData)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkJsonIterUnmarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkJsonIterMarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := Marshal2(toJsonData)
		if err != nil {
			panic(err)
		}
	}
}

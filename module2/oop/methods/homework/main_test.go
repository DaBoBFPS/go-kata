package mymain

import "testing"

type testpair struct {
	values  []float64
	average float64
}

var testsaverage = []testpair{
	{[]float64{1, 2}, 1.5},
	{[]float64{1, 1}, 1},
	{[]float64{-1, 1}, 0},
	{[]float64{0, 10}, 5},
	{[]float64{1.5, 1.5}, 1.5},
	{[]float64{3, 10}, 6.5},
	{[]float64{100.1, 9.3}, 54.699999999999996},
	{[]float64{-100, -10}, -55},
}
var testsum = []testpair{
	{[]float64{1, 2}, 3},
	{[]float64{1, 1}, 2},
	{[]float64{-1, 1}, 0},
	{[]float64{0, 10}, 10},
	{[]float64{1.5, 1.5}, 3},
	{[]float64{3, 10}, 13},
	{[]float64{100.1, 9.3}, 109.39999999999999},
	{[]float64{-100, -10}, -110},
}
var testmul = []testpair{
	{[]float64{1, 2}, 2},
	{[]float64{1, 1}, 1},
	{[]float64{-1, 1}, -1},
	{[]float64{0, 10}, 0},
	{[]float64{1.5, 1.5}, 2.25},
	{[]float64{3, 10}, 30},
	{[]float64{100.1, 9.3}, 930.9300000000001},
	{[]float64{-100, -10}, 1000},
}
var testdivide = []testpair{
	{[]float64{1, 2}, 0.5},
	{[]float64{1, 1}, 1},
	{[]float64{-1, 1}, -1},
	{[]float64{0, 10}, 0},
	{[]float64{1.5, 1.5}, 1},
	{[]float64{3, 10}, 0.3},
	{[]float64{100.1, 9.3}, 10.763440860215052},
	{[]float64{-100, -10}, 10},
}

func TestAverage(t *testing.T) {
	for _, pair := range testsaverage {
		v := average(pair.values[0], pair.values[1])
		if v != pair.average {
			t.Error(
				"For", pair.values,
				"expected", pair.average,
				"got", v,
			)
		}
	}
}
func TestSum(t *testing.T) {
	for _, pair := range testsum {
		v := sum(pair.values[0], pair.values[1])
		if v != pair.average {
			t.Error(
				"For", pair.values,
				"expected", pair.average,
				"got", v,
			)
		}
	}
}
func TestMul(t *testing.T) {
	for _, pair := range testmul {
		v := multiply(pair.values[0], pair.values[1])
		if v != pair.average {
			t.Error(
				"For", pair.values,
				"expected", pair.average,
				"got", v,
			)
		}
	}
}
func TestDivide(t *testing.T) {
	for _, pair := range testdivide {
		v := divide(pair.values[0], pair.values[1])
		if v != pair.average {
			t.Error(
				"For", pair.values,
				"expected", pair.average,
				"got", v,
			)
		}
	}
}

module gitlab.com/DaBoBFPS/go-kata

go 1.19

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/brianvoe/gofakeit v3.18.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/jmoiron/sqlx v1.3.5
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/tebeka/selenium v0.9.9
	gitlab.com/Vallyenfail/greet v0.0.0-20230215124749-3f2f6fc2f9e3
	golang.org/x/sync v0.1.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/stretchr/testify v1.8.3 // indirect
	golang.org/x/net v0.10.0 // indirect
)

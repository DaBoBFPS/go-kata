package main

import "encoding/json"

type Pet struct {
	ID        int        `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

func UnmarshalPet(data []byte) (Pet, error) {
	var j Pet
	err := json.Unmarshal(data, &j)
	return j, err
}

func (j *Pet) Marshal() ([]byte, error) {
	return json.Marshal(j)
}

type Category struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

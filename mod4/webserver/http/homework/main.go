package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/DaBoBFPS/go-kata/-/tree/main/mod4/webserver/http/homework/handlers"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func main() {
	port := ":8082"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	usersController := handlers.NewUserController()
	controller := handlers.NewController()
	r.Get("/users", usersController.GetUsers)
	r.Get("/users/{id}", usersController.GetUserByID)
	r.Get("/public/{filename}", controller.GetFile)
	r.Post("/", handlers.Hello)
	r.Post("/upload", controller.Upload)

	srv := &http.Server{Addr: port, Handler: r}

	go func() {
		log.Printf("server started on port %s", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit
	log.Println("Shutdown Server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
